# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

{
  description = "The morley-metadata flake";

  nixConfig.flake-registry = "https://gitlab.com/morley-framework/morley-infra/-/raw/main/flake-registry.json";

  inputs.morley-infra.url = "gitlab:morley-framework/morley-infra";

  outputs = { self, flake-utils, morley-infra, ... }:
    (flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = morley-infra.legacyPackages.${system};

        inherit (morley-infra.utils.${system}) ci-apps;

        # all local packages and their subdirectories
        # we need to know subdirectories for weeder and for cabal check
        local-packages = [
          { name = "morley-multisig"; subdirectory = "."; }
        ];

        # names of all local packages
        local-packages-names = map (p: p.name) local-packages;

        # source with gitignored files filtered out
        projectSrc = pkgs.haskell-nix.haskellLib.cleanGit {
          name = "morley-multisig";
          src = ./.;
        };

        # haskell.nix package set
        # parameters:
        # - release -- 'true' for master and producion branches builds, 'false' for all other builds.
        #   This flag basically disables weeder related files production, haddock and enables stripping
        # - commitSha, commitDate -- git revision info used during possible autodoc build
        # - optimize -- 'true' to enable '-O1' ghc flag, we intend to use it only in production branch
        hs-pkgs = { release, commitSha ? null, commitDate ? null }: pkgs.haskell-nix.stackProject {
          src = projectSrc;

          # use .cabal files for building because:
          # 1. haskell.nix fails to work for package.yaml with includes from the parent directory
          # 2. with .cabal files haskell.nix filters out source files for each component, so only the changed components will rebuild
          ignorePackageYaml = true;

          modules = [
            # common options for all local packages:
            {
              packages = pkgs.lib.genAttrs local-packages-names (packageName: ci-apps.collect-hie release {
                ghcOptions = [ "-O0" "-Werror" ];

                # enable haddock for local packages
                doHaddock = true;
              });
            }

            {
              # don't haddock dependencies
              doHaddock = false;

              # provide commit sha and date for morley-multisig in release mode:
              packages.morley-multisig = {
                preBuild = ''
                  export MORLEY_DOC_GIT_COMMIT_SHA=${if release then pkgs.lib.escapeShellArg commitSha else "UNSPECIFIED"}
                  export MORLEY_DOC_GIT_COMMIT_DATE=${if release then pkgs.lib.escapeShellArg commitDate else "UNSPECIFIED"}
                '';
              };
            }
          ];

          shell = {
            tools = {
              cabal = {};
              hlint = { version = "3.5"; };
              hpack = { version = "0.35.1"; };
            };
          };
        };

        hs-pkgs-development = hs-pkgs { release = false; };

        # component set for all local packages like this:
        # { morley = { library = ...; exes = {...}; tests = {...}; ... };
        #   morley-prelude = { ... };
        #   ...
        # }
        packages = pkgs.lib.genAttrs local-packages-names (packageName: hs-pkgs-development."${packageName}".components);

        flake = hs-pkgs-development.flake {};

      in pkgs.lib.lists.foldr pkgs.lib.recursiveUpdate {} [

        {
          utils = {
           inherit (morley-infra.utils.${system}) run-chain-tests;

            contracts-doc = { release, commitSha ? null, commitDate ? null }@releaseArgs: pkgs.runCommand "contracts-doc" {
              buildInputs = [
                (hs-pkgs releaseArgs).morley-multisig.components.exes.morley-multisig
              ];
            } ''
              mkdir $out
              cd $out
              mkdir autodoc
              morley-multisig document --name GenericWithCustomErrors --output \
                autodoc/GenericWithCustomErrors.md
            '';
          };
        }

        {
          legacyPackages = pkgs;

          packages = {
            haddock = with pkgs.lib; pkgs.linkFarmFromDrvs "haddock" (flatten (attrValues
              (mapAttrs (pkgName: pkg: optional (pkg ? library) pkg.library.haddock) packages)));

            all-components = pkgs.linkFarmFromDrvs "all-components" (builtins.attrValues flake.packages);

            default = self.packages.${system}.all-components;
          };

          checks = {
            # checks if all packages are appropriate for uploading to hackage
            run-cabal-check = morley-infra.utils.${system}.run-cabal-check { inherit local-packages projectSrc; };

            trailing-whitespace = pkgs.build.checkTrailingWhitespace ./.;

            reuse-lint = pkgs.build.reuseLint ./.;
          };
        }

        { inherit (flake) packages devShells apps; }

        {
          apps = ci-apps.apps {
            hs-pkgs = hs-pkgs-development;
            inherit local-packages projectSrc;
          };
        }
      ]));
}
