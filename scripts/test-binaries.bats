#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

@test "Morley multisig registry can print Multisig contracts" {
    morley-multisig print -n Specialized
    rm Specialized.tz
    morley-multisig print -n Generic
    rm Generic.tz
}
