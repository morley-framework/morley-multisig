-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

-- 'deriveRPC' creates orphans here
{-# OPTIONS_GHC -Wno-orphans #-}

module Test.Lorentz.Contracts.Multisig.Generic
  ( test_genericMultisig
  , test_documentation
  ) where

import Prelude hiding (drop)

import Test.Tasty (TestTree, testGroup)

import Lorentz hiding (assert)
import Lorentz.Contracts.Multisig.Generic as MSig
import Morley.Michelson.Interpret.Pack (packValue')
import Morley.Michelson.Untyped (Value'(ValueString))
import Morley.Tezos.Crypto
import Test.Cleveland
import Test.Cleveland.Lorentz (testLorentzDoc)
import Test.Cleveland.Michelson (runDocTests)

originateMultisig
  :: MonadCleveland caps m
  => Natural -> Natural -> [PublicKey]
  -> m (ContractHandle MSig.Parameter MSig.Storage ())
originateMultisig counter threshold pKeys = do
  originate "MultisigContract" (mkStorage counter threshold pKeys) $
    genericMultisigContract @'CustomErrors

data IdStringParameter = IdStringParameter MText
  deriving stock Generic
  deriving anyclass IsoValue

deriveRPC "Counter"
deriveRPC "Threshold"
deriveRPC "Keys"

deriving stock instance Eq CounterRPC
deriving stock instance Eq ThresholdRPC
deriving stock instance Eq KeysRPC

test_genericMultisig :: TestTree
test_genericMultisig = testGroup "tests to check genericMultisig contract functionality"
  [ testScenario "Call with enough amount of correct signatures \
             \with correct counter and 0 amount transfer" $ scenario do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      bs <- bsToSign msig counter simpleLambda
      signed <- signMany bs [bobSK, carlosSK]
      transfer msig $ calling (ep @"Main") $
        mkMultisigParam counter simpleLambda (Nothing : signed)
  , testScenario "Call with insufficient signatures" $ scenario do
      let
        counter = 0
        threshold = 3
      msig <- originateMultisig counter threshold masterPKList
      bs <- bsToSign msig counter simpleLambda
      signed <- signMany bs [bobSK, carlosSK]
      expectCustomError_ #insufficientSignatures $ transfer msig $ calling (ep @"Main") $
        mkMultisigParam counter simpleLambda (Nothing : signed)

  , testScenario "Call with incorrect counter" $ scenario do
      let
        counter = 1
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      bs <- bsToSign msig counter simpleLambda
      signed <- signMany bs [bobSK, carlosSK]
      expectCustomError_ #counterDoesntMatch $ transfer msig $ calling (ep @"Main") $
        mkMultisigParam (counter + 1) simpleLambda (Nothing : signed)

  , testScenario "Provide invalid signature" $ scenario do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      bs <- bsToSign msig counter simpleLambda
      signed <- signMany bs [aliceSK, carlosSK]
      expectCustomError_ #invalidSignature $ transfer msig $ calling (ep @"Main") $
        mkMultisigParam counter simpleLambda (Nothing : signed)

  , testScenario "Provide less signatures than public keys in storage"
    $ scenario do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      bs <- bsToSign msig counter simpleLambda
      signed <- signMany bs [aliceSK, bobSK]
      expectCustomError_ #fewerSignaturesThanKeys $ transfer msig $ calling (ep @"Main") $
        mkMultisigParam counter simpleLambda signed

  , testScenario "Provide more signatures than public keys in storage" $ scenario do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      bs <- bsToSign msig counter simpleLambda
      signed <- signMany bs [bobSK, carlosSK, aliceSK]
      expectCustomError_ #uncheckedSignaturesLeft $
        transfer msig $ calling (ep @"Main") $
          mkMultisigParam counter simpleLambda (Nothing : signed)

  , testScenario "Non-zero transfer to ParameterMain" $ scenario do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      bs <- bsToSign msig counter simpleLambda
      signed <- signMany bs [bobSK, carlosSK]
      expectCustomError_ #nonZeroTransfer $ transfer msig [tz|1u|] $ calling (ep @"Main") $
        mkMultisigParam counter simpleLambda $ Nothing : signed

  , testScenario "Non-zero transfer to Default" $ scenario do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      transfer msig [tz|1u|]

  , testScenario "Simple contract interaction example test" $ scenario do
      idContract <- importUntypedContract "resources/id.tz"
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      idAddr <- originate "idString" (ValueString [mt|kek|]) idContract
      let
        newStorage = [mt|mda|]
        lambdaParam = contractToLambda @MText (EpAddress idAddr DefEpName)
                      newStorage
      bs <- bsToSign msig counter $ Operation lambdaParam
      signed <- signMany bs [bobSK, carlosSK]
      transfer msig $ calling (ep @"Main") $
        mkMultisigParam counter (Operation lambdaParam) (Nothing : signed)
      st <- getStorage @MText idAddr
      assert (st == newStorage) "idString storage wasn't updated"

  , testScenario "Update threshold to zero is prohibited" $ scenario do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      let
        keyRotation = ChangeKeys (Threshold 0, Keys updatedPKList)
      bs <- bsToSign msig counter keyRotation
      signed <- signMany bs [bobSK, carlosSK]
      expectCustomError_ #zeroThresholdUpdate $ transfer msig $ calling (ep @"Main") $
        mkMultisigParam counter keyRotation (Nothing : signed)

  , testScenario "Update to threshold larger than keys list size is prohibited" $ scenario do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      let
        keyRotation = ChangeKeys (Threshold 4, Keys updatedPKList)
      bs <- bsToSign msig counter keyRotation
      signed <- signMany bs [bobSK, carlosSK]
      expectCustomError_ #thresholdLargerThanKeysSize $ transfer msig $ calling (ep @"Main") $
        mkMultisigParam counter keyRotation (Nothing : signed)

  -- TODO: use non-emulated once https://gitlab.com/morley-framework/morley/-/issues/562 is resolved
  , testScenario "Valid key rotation" $ scenario do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      let
        keyRotation = ChangeKeys (Threshold 2, Keys updatedPKList)
      bs <- bsToSign msig counter keyRotation
      signed <- signMany bs [bobSK, carlosSK]
      transfer msig $ calling (ep @"Main") $
        mkMultisigParam counter keyRotation (Nothing : signed)
      st <- getStorage @Storage msig
      assert (st == (CounterRPC 1, (ThresholdRPC 2, KeysRPC updatedPKList))) "Key list wasn't updated"
  ]
  where
    mkMultisigParam
      :: Natural -> ParamAction
      -> [Maybe $ TSignature (Packed ToSign)] -> ParamMain
    mkMultisigParam counter action sigs =
      ( #payload :! (Counter counter, #action :! action)
      , Signatures sigs
      )
    simpleLambda :: ParamAction
    simpleLambda = Operation $ mkLambda $ drop # nil
    bsToSign
      :: forall caps m. MonadCleveland caps m
      => ContractHandle MSig.Parameter MSig.Storage () -> Natural
      -> ParamAction
      -> m (Packed ToSign)
    bsToSign msigAddr counter action = do
      cId <- getChainId
      return $ Packed $ packValue' $ toVal ((cId, toAddress msigAddr), (counter, action))

    aliceSK = detSecretKey "\001\002\003\004"
    bobSK = detSecretKey "\005\006\007\008"
    carlosSK = detSecretKey "\009\010\011\012"
    zyabaSK = detSecretKey "\013\014\015\016"
    zyobaSK = detSecretKey "\017\018\019\020"

    alicePK = toPublic aliceSK
    bobPK = toPublic bobSK
    carlosPK = toPublic carlosSK
    zyabaPK = toPublic zyabaSK
    zyobaPK = toPublic zyobaSK

    masterPKList = [alicePK, bobPK, carlosPK]
    updatedPKList = [alicePK, zyabaPK, zyobaPK]

    signMany bs = runIO . traverse (\sk -> Just <$> lSign sk bs)

test_documentation :: [TestTree]
test_documentation =
  [ testGroup "Base errors" $
      runTests $ genericMultisigContract @'BaseErrors
  , testGroup "Custom errors" $
      runTests $ genericMultisigContract @'CustomErrors
  ]
  where
    runTests = runDocTests testLorentzDoc
