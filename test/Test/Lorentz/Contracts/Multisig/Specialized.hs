-- SPDX-FileCopyrightText: 2021 Oxhead Alpha
-- SPDX-License-Identifier: LicenseRef-MIT-OA

module Test.Lorentz.Contracts.Multisig.Specialized
  ( test_specializedMultisig
  ) where

import Prelude hiding (drop)

import Data.Coerce (coerce)
import Test.Tasty (TestTree, testGroup)

import Lorentz hiding (assert)
import Lorentz.Contracts.Multisig.Generic qualified as Generic
import Lorentz.Contracts.Multisig.Specialized as Specialized
import Morley.Michelson.Untyped.Value (Value'(..))
import Morley.Tezos.Crypto
import Test.Cleveland
import Test.Cleveland.Lorentz.Consumer

type TargetParameter = Natural

originateTarget
  :: forall param caps m.
    (MonadCleveland caps m, NiceParameterFull param) => m (ContractHandle param () ())
originateTarget =
  originate "TargetContract" () $ defaultContract $ cdr # nil # pair

originateMultisig
  :: forall param mainparam mname caps m.
    (NiceMultisigParam param mainparam mname, MonadCleveland caps m)
  => Natural -> Natural -> [PublicKey]
  -> EntrypointRef mname
  -> m (ContractHandle (Parameter param mainparam) Storage ())
originateMultisig counter threshold pKeys epref =
  originate "SpecializedMultisigContract" (mkStorage counter threshold pKeys) $
    specializedMultisigContract @param @mainparam @'Generic.CustomErrors epref

prepareContracts
  :: (MonadCleveland caps m)
  => Natural
  -> Natural
  -> [PublicKey]
  -> m ( ContractHandle (Parameter TargetParameter TargetParameter)
                         Storage ()
       , ContractHandle TargetParameter () ()
       )
prepareContracts counter threshold pKeys = do
  target <- originateTarget @TargetParameter
  msig <- originateMultisig @TargetParameter @TargetParameter @'Nothing counter threshold pKeys CallDefault
  pure (msig, target)

bsToSign :: forall param mainparam caps m.
     (MonadCleveland caps m, NicePackedValue (GenericMultisigAction param mainparam))
  => ContractHandle (Parameter param mainparam) Storage ()
  -> Natural
  -> GenericMultisigAction param mainparam
  -> m (Packed (ToSign param mainparam))
bsToSign msigAddr counter pl = do
  cId <- getChainId
  return $ lPackValue ((cId, toAddress msigAddr), (Counter counter, pl))

test_specializedMultisig :: TestTree
test_specializedMultisig = testGroup "tests to check genericMultisig contract functionality"
  [ testScenario "Call with enough amount of correct signatures \
             \with correct counter and 0 amount transfer" $ scenario do
      let
        counter = 0
        threshold = 2
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, toTAddress target)
      bs <- bsToSign msig counter action
      signed <- signMany bs [bobSK, carlosSK]
      transfer msig $ calling (ep @"MainParameter") $
        mkMultisigParam counter action $ Generic.Signatures (Nothing : signed)

  , testScenario "Call with insufficient signatures" $ scenario do
      let
        counter = 0
        threshold = 3
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, toTAddress target)
      bs <- bsToSign msig counter action
      signed <- signMany bs [bobSK, carlosSK]
      expectCustomError_ #insufficientSignatures $
        transfer msig $ calling (ep @"MainParameter") $
          mkMultisigParam counter action $ Generic.Signatures (Nothing : signed)

  , testScenario "Call with incorrect counter" $ scenario do
      let
        counter = 1
        threshold = 2
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, toTAddress target)
      bs <- bsToSign msig counter action
      signed <- signMany bs [bobSK, carlosSK]
      expectCustomError_ #counterDoesntMatch $
        transfer msig $ calling (ep @"MainParameter") $
          mkMultisigParam (counter + 1) action $ Generic.Signatures (Nothing : signed)

  , testScenario "Provide invalid signature" $ scenario do
      let
        counter = 0
        threshold = 2
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, toTAddress target)
      bs <- bsToSign msig counter action
      signed <- signMany bs [aliceSK, carlosSK]
      expectCustomError_ #invalidSignature $ transfer msig $ calling (ep @"MainParameter") $
        mkMultisigParam counter action (Generic.Signatures $ Nothing : signed)

  , testScenario "Provide less signatures than public keys in storage" $ scenario do
      let
        counter = 0
        threshold = 2
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, toTAddress target)
      bs <- bsToSign msig counter action
      signed <- signMany bs [aliceSK, bobSK]
      expectCustomError_ #fewerSignaturesThanKeys $
        transfer msig $ calling (ep @"MainParameter") $
          mkMultisigParam counter action (Generic.Signatures signed)

  , testScenario "Provide more signatures than public keys in storage" $ scenario do
      let
        counter = 0
        threshold = 2
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, toTAddress target)
      bs <- bsToSign msig counter action
      signed <- signMany bs [bobSK, carlosSK, aliceSK]
      expectCustomError_ #uncheckedSignaturesLeft $
        transfer msig $ calling (ep @"MainParameter") $
          mkMultisigParam counter action (Generic.Signatures $ Nothing : signed)
  , testScenario "Non-zero transfer with Operation action updates target balance" $ scenario do
      let
        counter = 0
        threshold = 2
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, toTAddress target)
      bs <- bsToSign msig counter action
      signed <- signMany bs [bobSK, carlosSK]
      transfer msig [tz|10u|] $ calling (ep @"MainParameter") $
        mkMultisigParam counter action $ Generic.Signatures (Nothing : signed)
      targetBalance <- getBalance target
      assert (targetBalance == [tz|10u|]) "Target balance wasn't updated"

  , testScenario "Non-zero transfer to Default updates multisig balance" $ scenario do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig @TargetParameter @TargetParameter
        counter threshold masterPKList CallDefault
      transfer msig [tz|10u|]
      msigBalance <- getBalance msig
      assert (msigBalance == [tz|10u|]) "msig balance wasn't updated"

  , testScenario "Simple contract interaction example test" $ scenario do
      idContract <- importUntypedContract "resources/id.tz"
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig @MText @MText counter threshold masterPKList CallDefault
      idAddr <- originate "idString" (ValueString [mt|kek|]) idContract
      let
        newStorage = [mt|mda|]
        action = Operation ([mt|mda|], (TAddress @MText $ toAddress idAddr))
      bs <- bsToSign msig counter action
      signed <- signMany bs [bobSK, carlosSK]
      transfer msig $ calling (ep @"MainParameter") $
        mkMultisigParam counter action (Generic.Signatures $ Nothing : signed)
      st <- getStorage @MText idAddr
      assert (st == newStorage) "idString storage wasn't updated"

  , testScenario "Bad key update with threshold > key count is rejected" $ scenario do
      let
        counter = 0
        threshold = 2
      (msig, _) <- prepareContracts counter threshold masterPKList
      let
        action = ChangeKeys (10, Generic.Keys masterPKList)
      bs <- bsToSign msig counter action
      signed <- signMany bs [bobSK, carlosSK]
      expectCustomError_ #thresholdLargerThanKeysSize $
        transfer msig $ calling (ep @"MainParameter") $
          mkMultisigParam counter action $ Generic.Signatures (Nothing : signed)

  , testScenario "Bad key update with threshold = 0 is rejected" $ scenario do
      let
        counter = 0
        threshold = 2
      (msig, _) <- prepareContracts counter threshold masterPKList
      let
        action = ChangeKeys (0, Generic.Keys masterPKList)
      bs <- bsToSign msig counter action
      signed <- signMany bs [bobSK, carlosSK]
      expectCustomError_ #zeroThresholdUpdate $
        transfer msig $ calling (ep @"MainParameter") $
          mkMultisigParam counter action $ Generic.Signatures (Nothing : signed)

  , testScenario "Key update with good threshold is accepted" $ scenario do
      let
        counter = 0
        threshold = 2
      (msig, _) <- prepareContracts counter threshold masterPKList
      let
        action = ChangeKeys (3, Generic.Keys masterPKList)
      bs <- bsToSign msig counter action
      signed <- signMany bs [bobSK, carlosSK]
      transfer msig $ calling (ep @"MainParameter") $ mkMultisigParam counter action $
        Generic.Signatures (Nothing : signed)

  , testScenario "Non-zero transfer with ChangeKeys action prohibited" $ scenario do
      let
        counter = 0
        threshold = 2
      (msig, _) <- prepareContracts counter threshold masterPKList
      let
        action = ChangeKeys (3, Generic.Keys masterPKList)
      bs <- bsToSign msig counter action
      signed <- signMany bs [bobSK, carlosSK]
      expectCustomError_ #nonZeroTransfer $ transfer msig [tz|10u|] $
        calling (ep @"MainParameter") $
          mkMultisigParam counter action $ Generic.Signatures (Nothing : signed)

  , testScenario "Mutez transfer updates target balance with sufficient amount of valid signatures"
    $ scenario do
      let
        counter = 0
        threshold = 2
      (msig, _) <- prepareContracts counter threshold masterPKList
      consumer <- originate "consumer" def $ contractConsumer @()
      let
        action = TransferTokens (toTAddress consumer, [tz|80u|])
      bs <- bsToSign msig counter action
      signed <- signMany bs [bobSK, carlosSK]
      transfer msig [tz|100u|]
      transfer msig $ calling (ep @"MainParameter") $ mkMultisigParam counter action $
        Generic.Signatures (Nothing : signed)
      consumerBalance <- getBalance consumer
      assert (consumerBalance == [tz|80u|]) "Consumer balance wasn't updated"

  , testScenario "Mutez transfer updates target balance with insufficient amount of valid \
                        \signatures and fails" $ scenario do
      let
        counter = 0
        threshold = 2
      (msig, _) <- prepareContracts counter threshold masterPKList
      consumer <- originate "consumer" def $ contractConsumer @()
      let
        action = TransferTokens (toTAddress consumer, [tz|80u|])
      bs <- bsToSign msig counter action
      bs1 <- bsToSign msig (counter + 1) action
      transfer msig [tz|100u|]
      signed <- signMany bs [bobSK, carlosSK]
      transfer msig $ calling (ep @"MainParameter") $ mkMultisigParam counter action $
        Generic.Signatures (Nothing : signed)
      signed1 <- signMany bs1 [carlosSK]
      expectCustomError_ #insufficientSignatures $
        transfer msig $ calling (ep @"MainParameter") $
          mkMultisigParam (counter + 1) action $
            Generic.Signatures (Nothing : Nothing : signed1)

  , testScenario "Non-zero transfer with TransferTokens action permitted" $ scenario do
      let
        counter = 0
        threshold = 2
      (msig, _) <- prepareContracts counter threshold masterPKList
      consumer <- originate "consumer" def $ contractConsumer @()
      let
        -- TODO: ensure that BALANCE includes AMOUNT once https://gitlab.com/morley-framework/morley/-/issues/121
        -- is resolved
        action = TransferTokens (toTAddress consumer, [tz|80u|])
      bs <- bsToSign msig counter action
      signed <- signMany bs [bobSK, carlosSK]
      transfer msig [tz|100u|]
      transfer msig [tz|10u|] $ calling (ep @"MainParameter") $
        mkMultisigParam counter action $ Generic.Signatures (Nothing : signed)
      consumerBalance <- getBalance consumer
      assert (consumerBalance == [tz|80u|]) "Consumer balance wasn't updated"
  ]
  where
    mkMultisigParam
      :: forall p mainparam. Natural
      -> GenericMultisigAction p mainparam
      -> Generic.Signatures
      -> (MainParams p mainparam)
    mkMultisigParam counter action (Generic.Signatures sigs) =
      ((Counter counter, action) , coerce sigs)

    aliceSK = detSecretKey "\001\002\003\004"
    bobSK = detSecretKey "\005\006\007\008"
    carlosSK = detSecretKey "\009\010\011\012"

    alicePK = toPublic aliceSK
    bobPK = toPublic bobSK
    carlosPK = toPublic carlosSK

    masterPKList = [alicePK, bobPK, carlosPK]

    signMany bs = runIO . traverse (\sk -> Just . coerce <$> lSign sk bs)
